LDLIBS := -lsqlite3 -lstdc++fs
CPPFLAGS := -std=c++17

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

vard: vard.cpp
	g++ ${CPPFLAGS} vard.cpp -o bin/vard $(LDLIBS)
	sudo setcap cap_setuid=ep bin/vard

trad: trad.cpp
	g++ ${CPPFLAGS} trad.cpp -o bin/trad $(LDLIBS)

all: trad vard

install: vard trad
	cp -a bin/vard $(PREFIX)/bin/
	cp -a bin/trad $(PREFIX)/bin/
	cp vard.service /usr/lib/systemd/system
