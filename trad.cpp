#include <cstdlib>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <map>
#include <set>
#include <sqlite3.h>
#include <string>
#include <time.h>

#include "include/config.hpp"
#include "include/cxxopts.hpp"

namespace vt = vardtrad;

cxxopts::ParseResult parse_or_help(cxxopts::Options& opts, int argc, char** argv){
  try{
    auto result = opts.parse(argc,argv);
    if (result.count("help")) {
      std::cout << opts.help() << std::endl;
    }
    return result;
  }
  catch(...){
    std::cout << opts.help() << std::endl;
    exit(EXIT_FAILURE);
  }
  return opts.parse(argc,argv);
}

int print_trad_result(void *a_param, int argc, char **argv, char **column){
  for (int i=0; i< argc; i++){
    if(strcmp(column[i],"timestart")==0){
      auto t=strtol(argv[i],NULL,10);
      auto str = std::string(asctime(gmtime(&t)));
      str.pop_back();
      printf("%s (UTC),\t", str.c_str());
    }
    else{
      printf("%s,\t", argv[i]);
    }
  }
  printf("\n");
  return 0;
}

int print_filename_result(void *a_param, int argc, char **argv, char **column){
  for (int i=0; i< argc; i++){
    if(strcmp(column[i],"timestart")==0){
      auto t=strtol(argv[i],NULL,10);
      auto str = std::string(asctime(gmtime(&t)));
      str.pop_back();
      printf("%s (UTC),\t", str.c_str());
    }
    else{
      printf("%s,\t", argv[i]);
    }
  }
  printf("\n");
  return 0;
}

int print_process_result(void *a_param, int argc, char **argv, char **column){
  for (int i=0; i< argc; i++){
    if(strcmp(column[i],"timestart")==0){
      auto t=strtol(argv[i],NULL,10);
      auto str = std::string(asctime(gmtime(&t)));
      str.pop_back();
      printf("%s (UTC),\t", str.c_str());
    }
    else{
      printf("%s,\t", argv[i]);
    }
  }
  printf("\n");
  return 0;
}

bool parseProcessOrFilename(std::string input, bool& is_process, bool& is_filename, std::string& input_f, std::string& prj_name, std::string& prj_dir){
  input_f = vt::absolute_path(input);
  auto prj2dir = vt::read_project_config();

  is_process=false;
  is_filename=false;

  for( auto prj: prj2dir){
    if (strncmp(input.c_str(), prj.first.c_str(), prj.first.size())==0){
      prj_name = prj.first;
      prj_dir = prj.second;
      is_process=true;
      break;
    }
    if (strncmp(input_f.c_str(), prj.second.c_str(), prj.second.size())==0){
      prj_name = prj.first;
      prj_dir = prj.second;
      input_f.erase(0,prj_dir.size());
      is_filename=true;
      break;
    }
  } 
  
  return is_filename or is_process;
}

void show(cxxopts::ParseResult& args){
  auto input = args["input"].as<std::string>();
  bool is_process=false;
  bool is_filename=false;
  std::string prj_name, prj_dir, input_f;
  
  if (not parseProcessOrFilename(input, is_process, is_filename, input_f, prj_name, prj_dir)){
    std::cout << "Not a valid file or hash." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // at this point connect to correct database
  sqlite3* db;
  sqlite3_open((prj_dir+".vardtrad/vardtrad.db").c_str(), &db);

  if( is_filename ){
    std::cout << "# Id, filename, description, format" << std::endl;
    sqlite3_exec(db,("SELECT * FROM file WHERE path=\""+input_f+"\";").c_str(),print_filename_result,NULL,NULL);
    std::cout << std::endl;
    std::cout << "# hash, direction" << std::endl;
    sqlite3_exec(db,("SELECT hash,direction,timestart FROM trad WHERE filename=\""+input_f+"\";").c_str(),print_filename_result,NULL,NULL);
    
  }
  else if( is_process ){
    sqlite3_exec(db,("SELECT * FROM process WHERE hash=\""+input+"\";").c_str(),print_process_result,NULL,NULL);
    std::cout << std::endl;
    std::cout << "# filename, direction" << std::endl;
    sqlite3_exec(db,("SELECT filename,direction,timestart FROM trad WHERE hash=\""+input+"\";").c_str(),print_filename_result,NULL,NULL);
  }

}


void meta(cxxopts::ParseResult& args){
  auto input = args["input"].as<std::string>();
  bool is_process=false;
  bool is_filename=false;
  std::string prj_name, prj_dir, input_f;
  
  if (not parseProcessOrFilename(input, is_process, is_filename, input_f, prj_name, prj_dir)){
    std::cout << "Not a valid file or hash." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  auto append = args["append"].as<bool>();
  
  // at this point connect to correct database
  sqlite3* db;
  sqlite3_open((prj_dir+".vardtrad/vardtrad.db").c_str(), &db);

  if( is_filename ){
    auto message=args["message"].as<std::string>();
    auto header=args["format"].as<std::string>();
    
    std::cout << message << " " << header << " " << append << std::endl;
    
    if(message.size()){
      if(append){
        sqlite3_exec(db,("UPDATE file SET description = description || '; "+message+"' WHERE path=\""+input_f+"\";").c_str(),NULL,NULL,NULL);
      }
      else{
        sqlite3_exec(db,("UPDATE file SET description = \""+message+"\" WHERE path=\""+input_f+"\";").c_str(),NULL,NULL,NULL);
      }
    }
    if(header.size()){
      sqlite3_exec(db,("UPDATE file SET header = \""+header+"\" WHERE path=\""+input_f+"\";").c_str(),NULL,NULL,NULL);
    }
  }
  else if( is_process ){
    auto message=args["message"].as<std::string>();
    if(message.size()){
      if(append){
        sqlite3_exec(db,("UPDATE process SET description = description || '; "+message+"' WHERE hash=\""+input+"\";").c_str(),NULL,NULL,NULL);
      }
      else{
        sqlite3_exec(db,("UPDATE process SET description = \""+message+"\" WHERE hash=\""+input+"\";").c_str(),NULL,NULL,NULL);
      }
    }
  }
  
}

int main(int argc, char** argv){
  
  // Parse subcommand names
  std::set<std::string> accepted_options = {"show","meta"};
  if (argc<2 or argc>1 and accepted_options.find(std::string(argv[1]))==accepted_options.end()){
    std::cout << "Usage: trad [command] <options>\n";
    std::cout << "Valid commands are: 'show', 'meta'.\n";
    std::cout << "\t'show': display information about specified file or process.\n";
    std::cout << "\t'meta': add metadata to file or process.\n";
    std::cout << "For help on specific command type 'trad [command] -h'" << std::endl;
    return 1;
  }
  
  // Modify argv->args for cxxopts parsers not to fail in recognizing options 
  char** args=new char*[argc-1];
  for(uint i1=1,i2=0; i1<argc; ++i1,++i2){
    args[i2]=argv[i1];
  }
  argc-=1;
  
  
  // parser for "show" subcommand
  if(strcmp(argv[1],"show")==0){
    cxxopts::Options options("trad show", "Query vardtrad database.");
    options.add_options()
      ("h,help","Print help");
    options.add_options()
      ("i,input","File name or process hash",cxxopts::value<std::string>());
    auto opts=parse_or_help(options,argc,args);
    show(opts);
  }
  
  // parser for "meta" subcommand
  else if(strcmp(argv[1],"meta")==0){
    cxxopts::Options options("trad meta", "Add metadata to vardtrad database. (NOT IMPLEMENTED YET)");
    options.add_options()
      ("h,help","Print help");
    options.add_options()
      ("i,input","File name or process hash (required)",cxxopts::value<std::string>())
      ("m,message","File/Process description (at list 'm' or 'f' must be specified)",cxxopts::value<std::string>()->default_value(""))
      ("a,append","Append message instead of overwriting it")
      ("f,format","File format of header in case of tabular data (at list 'm' or 'f' must be specified)",cxxopts::value<std::string>()->default_value(""));
      
    auto opts=parse_or_help(options,argc,args);
    meta(opts);
  }
  
  return 0;
}
