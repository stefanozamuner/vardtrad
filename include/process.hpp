#ifndef VARDTRAD_PROCESS_HPP
#define VARDTRAD_PROCESS_HPP

#include <set>
#include <signal.h>
#include <string>

namespace vardtrad{

// check if a given process is running 
bool pid_is_running(uint pid) {
//  while(waitpid(-1, 0, WNOHANG) > 0) {}
  if ( 0==kill(pid, 0) ){
    return 1; 
  }
  return 0;
}


// internally a process is recognized by its pid and saved in memory in order to easily retrieve
// starttime and has value when needed. This is probably not the safest option (pids are reused)
// but we take care of deleting the pid from memory when it dies (hopefully) so we should be fine.
struct running_process_info_t{
  uint pid;
  uint starttime;
  std::string hash;
  std::set<std::string> read,write;
  
  running_process_info_t(uint p=0, uint s=0, std::string h=""): pid(p),starttime(s),hash(h){}
};

}

#endif
