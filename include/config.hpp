#ifndef VARDTRAD_CONFIG_HPP
#define VARDTRAD_CONFIG_HPP

#include <fstream>
#include <map>
#include <string>
#include <vector>

#include "string.hpp"

namespace vardtrad{

const char* prj_conf_filename="$HOME/.vardtrad/projects.conf";
const char* ignore_conf_filename="$HOME/.vardtrad/ignore.conf";

typedef std::map<std::string,std::string> prjconf_t;
typedef std::map<std::string,std::vector<std::string>> ignore_t;

prjconf_t read_project_config(){
  prjconf_t prj2dir;
  std::ifstream prj_conf(shellExpand(prj_conf_filename));  
  std::string prj_name, prj_dir;
  while(prj_conf >> prj_name >> prj_dir){
    if(prj_dir.back()!='/'){
      prj_dir+="/";
    }
    prj2dir[prj_name]=prj_dir;
  }
  prj_conf.close();
  return prj2dir;
}

ignore_t read_ignore_config(prjconf_t& prj2dir){
  std::string name;
  // read default ignore instruction
  std::vector<std::string> ignore_always = {".vardtrad/",".git/"};
  std::ifstream ignore_conf(shellExpand(ignore_conf_filename));  
  while(ignore_conf >> name){
    ignore_always.push_back(name);
  }
  ignore_conf.close();
  
  // now parse every .vardtradignore file in project directories
  ignore_t ignore;
  for(auto prj: prj2dir){
    ignore[prj.first]={};
    for (auto ign: ignore_always){
      ignore[prj.first].push_back(ign);
    }
    // TODO
  }
  return ignore;
}

}
#endif
