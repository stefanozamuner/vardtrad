#ifndef VARDTRAD_VARD_HPP
#define VARDTRAD_VARD_HPP

#include <filesystem>
#include <map>
#include <memory>
#include <sys/types.h>
#include <sqlite3.h>
#include <string>
#include <unistd.h>
#include <vector>

#include "config.hpp"
#include "sqlite3.hpp"

namespace fs = std::filesystem;

namespace vardtrad{

class vard_t{
  private:
  std::map<uint,running_process_info_t> rpid; // contains info about running process identifiers
  std::string sysdig_cmd;
  prjconf_t prj_dir;
  sqlite3** conn;
  std::map<std::string,sqlite3*> db;
  ignore_t ignore;
  
  public:
  vard_t(const prjconf_t prj2dir, const ignore_t ign ){
    
    prj_dir.insert(prj2dir.begin(), prj2dir.end());
    ignore.insert(ign.begin(),ign.end());
    
    // compute sysdig command
    sysdig_cmd = sysdig_run_command(prj_dir);
    
    // open connection to databases and eventually create tables
    conn = new sqlite3*[prj2dir.size()];
    uint n=0;
    for(auto prj: prj2dir){
      // check if folder .vardtrad exists or create it otherwise
      fs::path vardtrad_folder(prj.second+".vardtrad/");
      if (not fs::exists(vardtrad_folder)) fs::create_directory(vardtrad_folder);
      
      int rc=sqlite3_open((prj.second+".vardtrad/vardtrad.db").c_str(), &conn[n]);
      db[prj.first]=conn[n];
      n+=1;
     
		  int r0 = turn_foreign_key_on(db[prj.first]);
		  int r1 = create_table_file(db[prj.first]);
      int r2 = create_table_process(db[prj.first]);
      int r3 = create_table_trad(db[prj.first]);
    }
  }

  void store(event_t& e){
    store_event(db[e.project], e,rpid[e.pid]);
  }

  void register_running_process(event_t& e){
    // eventually remove dead processes
    std::vector<uint> dead;
    for(auto p : rpid){
      if( not pid_is_running(p.first) ){
        dead.push_back(p.first);
      }
    }
    for (auto p: dead){
      rpid.erase(p);
    }
    // add new pid
    rpid[e.pid]=running_process_info_t( e.pid,e.evt_rawtime_s,e.hash() );
  }

  void process(event_t evt){
    // if a new pid is encountered
    
    if (rpid.find(evt.pid)==rpid.end()){
      register_running_process(evt);
    }
    // neglect events relative to files to be ignored
    for (auto x: ignore[evt.project]){
      if (evt.filename.find(x)!=std::string::npos){
        return;
      }
    }
    // if the event is READ and the same file is not already an input for the process
    if (evt.evt_type==READ_EVENT_TYPE and rpid[evt.pid].read.find(evt.filename)==rpid[evt.pid].read.end()){
      rpid[evt.pid].read.insert(evt.filename);
      store(evt);
    }
    // same if it is a WRITE event
    else if (evt.evt_type==WRITE_EVENT_TYPE and rpid[evt.pid].write.find(evt.filename)==rpid[evt.pid].write.end()){
      rpid[evt.pid].write.insert(evt.filename);
      store(evt);
    }
  }

  void run(){
    std::array<char, 128> buffer;
    std::string result;
    
    int uid = getuid();
    int gid = getgid();
    setgid(0);
    setuid(0);
    std::shared_ptr<FILE> pipe(popen(sysdig_cmd.c_str(), "r"), pclose);
    setgid(gid);
    setuid(uid);
    
    if (!pipe){
      throw std::runtime_error("popen() failed!");
    }
    std::string sysdig_str="";
    while (!feof(pipe.get())) {
      if (fgets(buffer.data(), 128, pipe.get()) != nullptr){
        sysdig_str+=buffer.data();
        sysdig_str.erase(std::remove(sysdig_str.begin(), sysdig_str.end(), '\n'), sysdig_str.end());
        sysdig_str.erase(std::remove(sysdig_str.begin(), sysdig_str.end(), '\r'), sysdig_str.end());
        
        if (sysdig_str.back()=='#'){
          process( event_t(sysdig_str,prj_dir) );
          sysdig_str="";
        }
      }
    }
    
  }
  
};
}

#endif
