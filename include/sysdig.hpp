#ifndef VARDTRAD_SYSDIG_HPP
#define VARDTRAD_SYSDIG_HPP

#include <functional>
#include <sstream>
#include <string>
#include <vector>

#include "string.hpp"

namespace vardtrad{

// 'fd.name' absolute path to filename
// 'proc.pid' pid of the process
// 'proc.exeline' command line that started the process
// 'proc.cwd' current working directory of the process
// 'proc.env' environment variables of the process
// 'evt.type' event type e.g. open, close, write, dup, ...
// 'evt.rawtime.s' event absolute time (seconds from epoch)
std::string sysdig_p="\"%fd.name %proc.pid '%proc.exeline' '%proc.cwd' '%proc.env' %evt.type %evt.rawtime.s #\"";

// recognize if an event is a read or write
enum FILE_EVENT_TYPE: uint {OTHER_EVENT_TYPE=0,READ_EVENT_TYPE=1,WRITE_EVENT_TYPE=2};

uint parse_event_type(std::string evt_t){
  if (evt_t.find("read")!=std::string::npos){
    return READ_EVENT_TYPE;
  }  
  else if(evt_t.find("write")!=std::string::npos){
    return WRITE_EVENT_TYPE;
  }
  return OTHER_EVENT_TYPE;
}

// struct used to store a single event as returned by sysdig
// also normalize filenames and compute an unique hash describing the event
struct event_t{
  // fd
  std::string filename;
  // proc
  uint pid;
  std::string exeline;
  std::string cwd;
  std::string env;
  // evt
  uint evt_type;
  uint evt_rawtime_s;
  std::string project;
  
  event_t(std::string sysdig_str,const prjconf_t& prj2dir){
    std::vector<std::string> tok = tokenize(sysdig_str);
    filename=tok[0];
    pid=std::stoi(tok[1]);
    exeline=tok[2];
    cwd=tok[3];
    env=tok[4];
    evt_type=parse_event_type(tok[5]);
    evt_rawtime_s=std::stoi(tok[6]);
    // remove project folder from filename. This will allow the project folder to be moved around
    for(auto prj: prj2dir){
      if(filename.find(prj.second)!=std::string::npos){
        filename.erase(0,prj.second.size());
        project=prj.first;
        break;
      }
    }
  }
  
  std::string hash(){
    std::size_t const h1 ( std::hash<uint>{}(pid) );
    std::size_t const h2 ( std::hash<uint>{}(evt_rawtime_s) );
    auto h3 = h1 ^ (h2 << 1);
    std::stringstream sstream;
    sstream << std::hex << h3;
    return project+":"+sstream.str();
  }
};

std::string sysdig_run_command(prjconf_t& prj2dir){
  std::string contains = "\\(";
  uint prj_idx=0;
  for (auto prj: prj2dir){
    if(prj_idx!=0){
      contains+=" or";
    }
    contains+=" fd.name contains " + prj.second;
    prj_idx+=1;
  }
  contains+="\\)";
  return "sysdig -p " + sysdig_p + " fd.type=file and "+contains;
}

}

#endif
