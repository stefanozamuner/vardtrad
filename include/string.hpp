#ifndef VARDTRAD_STRING_HPP
#define VARDTRAD_STRING_HPP

#include <algorithm>
#include <filesystem>
#include <string>
#include <vector>
#include <wordexp.h>

namespace fs = std::filesystem;

namespace vardtrad{

// expand string as a bash would do. Used for expanding $HOME in configuration filenames
const char* shellExpand(const char* filename){
  wordexp_t p;
  char** w;
  wordexp(filename, &p, 0);
  w = p.we_wordv;
  return w[0];
}

// tokenize a string using spaces while avoiding splitting quoted strings. Used for parsing sysdig output
std::vector<std::string> tokenize(std::string str){
  std::vector<std::string> ret;
  ret.push_back("");  
  for( size_t i=0; i<str.length(); i++){
    char c = str[i];
    if( c==' ' ){
      ret.push_back("");
    }
    else if(c=='\"' or c=='\'' ){
      i++;
      while( str[i]!='\"' and str[i]!='\'' ){
        ret.back()+=str[i];
        i++; 
     }
    }
    else{
      ret.back()+=c;
    }
  }
  ret.erase( std::remove( ret.begin(), ret.end(), "" ), ret.end() );
  return ret;
}

// take a string describing a path and returns the corresponding absolute path
std::string absolute_path(std::string path){
  fs::path p(path);
  auto abs = fs::absolute(p);
  return abs.string();
}

}

#endif
