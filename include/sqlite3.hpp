#ifndef VARDTRAD_SQLITE3_HPP
#define VARDTRAD_SQLITE3_HPP

#include <iostream>
#include <sqlite3.h>
#include <string>

#include "process.hpp"
#include "sysdig.hpp"

namespace vardtrad{

std::string get_insert_into_file(event_t& e,running_process_info_t& rp){
  std::string ins="INSERT INTO file(path) VALUES ('";
  ins+=e.filename;
  ins+="');";
  return ins;
}

std::string get_insert_into_process(event_t& e,running_process_info_t& rp){
  std::string ins="INSERT INTO process(exeline,cwd,env,pid,timestart,hash) VALUES ('";
  ins+=e.exeline;
  ins+="','";
  ins+=e.cwd;
  ins+="','";
  ins+=e.env;
  ins+="',";
  ins+=std::to_string(e.pid);
  ins+=",";
  ins+=std::to_string(rp.starttime);
  ins+=",'";
  ins+=rp.hash;
  ins+="');";
  return ins;
}

std::string get_insert_into_trad(event_t& e,running_process_info_t& rp){
  std::string ins="INSERT INTO trad(pid,timestart,filename,hash,direction) VALUES (";
  ins+=std::to_string(e.pid);
  ins+=",";
  ins+=std::to_string(rp.starttime);
  ins+=",'";
  ins+=e.filename;
  ins+="','";
  ins+=rp.hash;
  if(e.evt_type==READ_EVENT_TYPE){
    ins+="','<');";
  }
  else if(e.evt_type==WRITE_EVENT_TYPE){
    ins+="','>');";
  }
  return ins;
}

// Turn foreign key support on
int turn_foreign_key_on(sqlite3* db){
  return sqlite3_exec(db,"PRAGMA foreign_keys=on;",NULL,NULL,NULL);
}

int create_table_file(sqlite3* db){
  // files are univocally identified by their path.
  // 'description' is a optional text describing the file content
  // 'header' can be used for data files to describe each column
  return sqlite3_exec(db,"CREATE TABLE IF NOT EXISTS file (id INTEGER PRIMARY KEY, path TEXT UNIQUE, description TEXT, header TEXT);",NULL,NULL,NULL); 
}      

int create_table_process(sqlite3* db){
  // processes are not univocally identified by their pid. For this reason an hash value based on pid and process start time is used.
  // 'exeline' contains the command line (w/ comments) that launched the process
  // 'cwd' current working directory when the command was launched
  // 'env' environment variables of the process
  // 'pid' pid of the process
  // 'timestart' estimated timestamp (in ms) in which the process was started
  // 'description' optional text describind the goal of the process 
  return sqlite3_exec(db,"CREATE TABLE IF NOT EXISTS process (exeline STRING, cwd TEXT, env TEXT, pid INTEGER, timestart INTEGER, description TEXT, hash TEXT UNIQUE, PRIMARY KEY(pid,timestart));",NULL,NULL,NULL);
}

int create_table_trad(sqlite3* db){      
  // trad contains the relationships between processes and files
  // 'pid' and 'timestamp' are used to identify the process
  // 'filename' is used to identify the corresponding file
  // 'direction' is either `>` (output) or `<`(input)
  return sqlite3_exec(db,"CREATE TABLE IF NOT EXISTS trad (pid INTEGER, timestart INTEGER, filename REFERENCES file(path) ON UPDATE CASCADE, direction CHAR(1), hash TEXT, FOREIGN KEY (pid,timestart) REFERENCES process(pid,timestart), PRIMARY KEY(pid,timestart,filename,direction));",NULL,NULL,NULL);
}

int store_event(sqlite3* db, event_t& e,running_process_info_t& rp){
  sqlite3_exec(db,get_insert_into_file(e,rp).c_str(),NULL,NULL,NULL);
  sqlite3_exec(db,get_insert_into_process(e,rp).c_str(),NULL,NULL,NULL);
  sqlite3_exec(db,get_insert_into_trad(e,rp).c_str(),NULL,NULL,NULL);
  return 0;
}

}
#endif
