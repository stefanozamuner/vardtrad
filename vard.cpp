#include <iostream>
#include <sys/types.h>

#include "include/config.hpp"
#include "include/vard.hpp"
 
namespace vt = vardtrad;

int main(){
  int uid = getuid();
  int gid = getgid();
  
  // read project configuration file
  auto prj2dir = vt::read_project_config();  

  // read exclude_regex configuration file
  auto ignore = vt::read_ignore_config(prj2dir);

  // run vard
  vt::vard_t vard(prj2dir,ignore);
  vard.run();
  
  return 0;
}
