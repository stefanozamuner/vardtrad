# VARDTRAD #

Vardtrad is a small program that helps you keeping track of your work.
It monitors one or more folders for i/o operations and store information
about the process responsible for that operation is an sqlite3 database.
You can easily query the database and even add text description that will 
help you and others remember/understand the workflow in your project.
